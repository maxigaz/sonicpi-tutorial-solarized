# Changelog

## v1.2.1

- Fixed value in metadata

## v1.2.0

- Option “Unstick Table of Contents”
	- Renamed from “Hide Table of Contents” to be more clear
	- Fixed the body being overly wide with this option on
	- Fixed issue with small screen/window width
	- Both the TOC and body are now centred in wider screens/windows
- Tweaked text colour for code snippets separately for dark and light mode

## v1.1.0

- Added option to hide Table of Contents
	- This doesn’t actually “hide” the TOC; instead, it places it on the top of the page so that you’ll need to scroll up to see it.
	- With very small screens/browser windows, it causes problems, so the option is disabled by default.
- Simplified light/dark mode switch
- Added option “Body scrollbar fix”
	- This hides the body scrollbar unless the screen/window width is 600px or less.
	- It’s enabled by default.

## v1.0.0

Initial release.
