# Sonic Pi Tutorial Solarized

A [Usercss](https://github.com/openstyles/stylus/wiki/Usercss) theme for [Sonic Pi’s online tutorial](https://sonic-pi.net/tutorial.html) with some optional extras. It uses the colour palette from [Solarized](https://ethanschoonover.com/solarized/).

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/sonicpi-tutorial-solarized/raw/master/sonicpi-tutorial-solarized.user.css)

## Screenshots

![Screenshot using Solarized Dark colours](screenshot-dark.png "Screenshot using Solarized Dark colours")
![Screenshot using Solarized Light colours](screenshot-light.png "Screenshot using Solarized Light colours")

## Install

1. Get a style manager. The style has been tested with [Stylus](https://add0n.com/stylus.html), although it is also compatible with [xStyle](https://github.com/FirefoxBar/xStyle). Other style managers might also work but are *not* supported.
2. Install the stylesheet by clicking [here](https://gitlab.com/maxigaz/sonicpi-tutorial-solarized/raw/master/sonicpi-tutorial-solarized.user.css) and a new window of Stylus should open, asking you to confirm to install the style.
3. Enjoy.

## Options

Currently, the following [options](https://github.com/openstyles/stylus/wiki/UserCSS#how-do-i-customize-usercss) are available in case you want to customise the style:

- Light Mode: changes the background and text colours based on the dark and light variations of Solarized
- Image brightness:
	- Specified in percentage. 80 by default so that it’s easier on the eye in the dark.
	- Regardless of this setting, when you hover the mouse over an image, the value always becomes 100.
- Unstick Table of Contents:
	- This places the TOC on the top of the page so that you’ll need to scroll up to see it. The purpose is to increase usability with small to medium device screens and browser windows.
	- Disabled by default.
- Body scrollbar fix:
	- This hides the body scrollbar unless the screen/window width is 600px or less.
	- Enabled by default.

## Reporting issues

Before you open a new issue ticket, please, make sure you’ve installed the **latest development version**. (Open the style from [here](https://gitlab.com/maxigaz/sonicpi-tutorial-solarized/raw/master/sonicpi-tutorial-solarized.css). If you have installed it earlier, tell Stylus to reinstall it, overriding the version you currently have.)

In the issue description, include the following:

- An example URL to the page you’re experiencing the problem on or provide step by step instructions on how to reproduce it.
- The version of your web browser, style manager, and the userstyle.
- In addition, including a screenshot or screencast of the problem is also helpful.

## Changelog

Changes between releases are summarized [here](https://gitlab.com/maxigaz/sonicpi-tutorial-solarized/blob/master/CHANGELOG.md).

## Credits

In almost all cases, the colour palette from [Solarized](https://ethanschoonover.com/solarized/) has been followed as an example.

This style wouldn’t have been made if it hadn’t been for Sonic Pi. Any contributors to Sonic Pi, if you’re reading this: Thank you very much for your work!

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
